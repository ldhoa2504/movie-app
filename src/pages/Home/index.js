import React from 'react'

import Player from '../../components/player/Player';

const Home = () => {
  return (
    <div>
      Home page
      <Player />
    </div>
  )
}

export default Home